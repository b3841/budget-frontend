# install and cache dependencies, build the app
FROM node:18.16.0-slim@sha256:09714f3334c1cda4ffac832880b57fc9c72253dd365ce7fa3ff1d1705aa9435a AS build
WORKDIR /usr/src/app
COPY package.json package-lock.json ./
RUN npm ci
COPY . .
RUN npm run build

# copy to nginx and run
FROM nginx:1.24@sha256:f3c37d8a26f7a7d8a547470c58733f270bcccb7e785da17af81ec41576170da8
EXPOSE 4200
COPY configs/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build --chown=nginx:nginx /usr/src/app/dist/budget-frontend-v3 /usr/share/nginx/html
ENTRYPOINT  ["nginx","-g","daemon off;"]
