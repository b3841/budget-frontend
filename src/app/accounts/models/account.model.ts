import {CurrencyEnum} from "../../models/enums/currency-enum.model";

export interface AccountModel{
  id: string
  name: string,
  currency: CurrencyEnum,
  savings: boolean
}
