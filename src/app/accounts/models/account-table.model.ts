import {CurrencyEnum} from "../../models/enums/currency-enum.model";

export interface AccountTableModel{

   id: string,
   name: string,
   amount: string,
   currency: CurrencyEnum,
   lastAction: string,
   isSavings: boolean
}
