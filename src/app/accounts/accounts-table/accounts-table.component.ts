import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {AccountsService} from "../../services/accounts.service";
import {AccountTableModel} from "../models/account-table.model";
import {AppToastService} from "../../_helpers/toast";
import {CurrencyEnum} from "../../models/enums/currency-enum.model";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {AccountModel} from "../models/account.model";
import {MovementTypeEnum} from "../../models/enums/movement-type.enum";

@Component({
  selector: 'app-accounts-table',
  templateUrl: './accounts-table.component.html',
  styleUrls: ['./accounts-table.component.css']
})
export class AccountsTableComponent implements OnInit {

  items: AccountTableModel[] = []
  @ViewChild('accountDialogTemplate')
  accountDialog: TemplateRef<any>;
  @ViewChild('accountDeleteModal')
  deleteDialog: TemplateRef<any>;
  activeModal: NgbModalRef;
  accountForm: FormGroup;
  accountId: string | null = null;

  currencyTypes = CurrencyEnum
  movementType = MovementTypeEnum

  constructor(
    private accountsService: AccountsService,
    private toast: AppToastService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
  }

  ngOnInit(): void {
    this.getTable()
    this.createForm()
  }

  getTable() {
    this.accountsService.getTable()
      .subscribe(
        (result) => {
          this.items = result
        },
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        }
      )
  }

  private createForm() {
    this.accountForm = this.fb.group({
      name: [null, [Validators.required]],
      currency: [CurrencyEnum.KZT, [Validators.required]],
      savings: [false, []]
    })
  }

  showModal() {
    this.activeModal = this.modalService.open(this.accountDialog, {centered: true});

    this.activeModal.result.finally(() => {
      this.accountId = null
      this.accountForm.reset()
    })
  }

  submit() {
    const body: AccountModel = {
      id: this.accountId,
      ...this.accountForm.getRawValue()
    }

    this.accountsService.save(body)
      .subscribe(
        (res) => {
          this.accountForm.reset()
          this.activeModal.dismiss()
          this.getTable()
        },
        (err) => {
          console.log(err)
          this.toast.defaultError(err.error)
        }
      )
  }

  edit(id: string) {
    this.accountsService.getOne(id)
      .subscribe(accountModel => {

          this.accountId = id

          this.accountForm.setValue({
            name: accountModel.name,
            currency: accountModel.currency,
            savings: accountModel.savings
          });

          this.showModal()
        },
        error => {
          console.log('err', error);
          this.toast.defaultError(error?.error)
        }
      )
  }

  askDelete(id: string) {
    this.modalService.open(this.deleteDialog, {centered: true})
      .result
      .then((res) => {
        if (res)
          this.accountsService
            .delete(id)
            .subscribe(
              () => this.getTable(),
              error => {
                console.log(error)
                this.toast.defaultError(error.error)
              })
      })
  }

}
