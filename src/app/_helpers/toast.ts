import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import {ErrorModel} from "./models/error.model";

export interface CommonToast {
  title: string;
  body: string;
}

@Injectable({ providedIn: 'root' })
export class AppToastService {
  constructor(private toastr: ToastrService) {}

  success(toast: CommonToast) {
    this.toastr.success(toast.body, toast.title);
  }

  error(toast: CommonToast, id?: string) {
    const code = id ? `(Код ошибки: ${id.split('-')[0]})` : '';
    this.toastr.error(`${toast.body} ${code}`, toast.title);
  }

  defaultError(error?: ErrorModel) {
    const code = error?.id ? `Код ошибки: ${error.id.split('-')[0]}` : '';
    if (error?.message) {
      const msg = error.message;
      this.error({
        title: 'Ошибка',
        body: `${msg} ${code}`,
      });
      return;
    } else {
      this.error({
        title: 'Ошибка',
        body: `Что-то пошло не так, попробуйте еще раз или обратитесь в службу поддержки ${code}`,
      });
    }
  }
}
