export function toIso1String(dateStruct: any): string {
  return dateStruct.year + '-' + pad(dateStruct.month) + '-' + pad(dateStruct.day)
}

export function pad(number: number) {
  if (number < 10) {
    return '0' + number;
  }
  return number.toString();
}

var map = new Map([
  [1, 31],
  [2, 28],
  [3, 31],
  [4, 30],
  [5, 31],
  [6, 30],
  [7, 31],
  [8, 31],
  [9, 30],
  [10, 31],
  [11, 30],
  [12, 31],
]);

export function getLastDayOfMonth(monthString: string, yearString: string) {
  let year = parseInt(yearString, 10)
  let month = parseInt(monthString, 10)
  if (year % 4 === 0 && month === 2)
    return 29
  else
    return map.get(month)
}


export function getLastDayOfMonthFromString(date: string) {
  const dateStruct = getDateStruct(date)
  const y = dateStruct.year
  const m = dateStruct.month
  return y + '-' + m + '-' + getLastDayOfMonth(m, y)
}

export function getTodayStruct() {
  const now = new Date()
  return {
    day: now.getDate(),
    month: now.getMonth() + 1,
    year: now.getFullYear()
  }
}

export function getTodayString() {
  const now = new Date()
  return now.getFullYear() + '-' + pad((now.getMonth() + 1)) + '-' + pad(now.getDate())
}

export function truncateMilliSeconds(time: string) {
  let indexOfDot = time.indexOf('.')
  if (indexOfDot != -1) {
    var c = time.substring(0, indexOfDot)
    console.log('truncateMilliSeconds was: ' + time + ', after: ' + c)
    return c
  } else {
    console.log('truncateMilliSeconds was: ' + time + ', after: ' + time)
    return time
  }
}

export function getDateStruct(value: string) {
  let date = value.split('-');
  // return {
  //   year: parseInt(date[0], 10),    // Зачем здесь Integer??
  //   month: parseInt(date[1], 10),
  //   day: parseInt(date[2], 10)
  // };

  return {
    year: date[0],    // Попробуем отдавать String
    month: date[1],
    day: date[2]
  };
}


