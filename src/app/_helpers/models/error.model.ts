export interface ErrorModel {
  id: string;
  status: string;
  code: string;
  error: string;
  data: string;
  detail: string;
  message?: string;
}
