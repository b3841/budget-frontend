export const TRANSFER_SENDER_CATEGORY_ID = 'cf8e15a9-0a45-44d5-9491-4a6414c38637'
export const TRANSFER_RECEIVER_CATEGORY_ID = 'bb817c86-665c-4469-92cf-4a6df55e8abb'
export const FEE_CATEGORY_ID = '9e97648d-79b1-44a7-b2eb-e48081c0b242'

export function isTransfer(categoryId: string) {
  return categoryId === TRANSFER_SENDER_CATEGORY_ID || categoryId === TRANSFER_RECEIVER_CATEGORY_ID
}

export function isTransferFee(categoryId: string) {
  return categoryId === FEE_CATEGORY_ID
}
