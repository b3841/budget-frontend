import {pad} from "./date.utils";

export function getTimeString(){
  let now = new Date();
  return 'T' + pad(now.getHours()) + ':' + pad(now.getMinutes()) + ':' + pad(now.getSeconds()) + '.' + now.getMilliseconds() + 'Z'
}
