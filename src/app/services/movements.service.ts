import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {MovementTypeEnum} from "../models/enums/movement-type.enum";
import {Observable} from "rxjs";
import {CategoryGroupModel} from "../movements/models/category-group.model";
import {CategoryGroupFilter} from "../movements/models/category-group-filter";
import {RangeTypeEnum} from "../models/enums/range-type.enum";
import {MovementModel} from "../movements/models/movement.model";
import {MovementTableModel} from "../movements/models/movement-table.model";

@Injectable({
  providedIn: 'root'
})
export class MovementsService {
  private API_PRIVATE_URL = 'api/private/v1/movements';

  constructor(private httpClient: HttpClient) {
  }

  getTable(filter: CategoryGroupFilter, type: MovementTypeEnum): Observable<CategoryGroupModel[]> {
    return this.httpClient.post<CategoryGroupModel[]>(this.API_PRIVATE_URL + '?type=' + type, filter)
  }

  getMovements(categoryId: string, filter: CategoryGroupFilter, type: MovementTypeEnum) : Observable<Map<string,MovementTableModel[]>> {
    return this.httpClient.post<Map<string,MovementTableModel[]>>(this.API_PRIVATE_URL + '/movement?type=' + type + '&categoryId=' + categoryId, filter)
  }

  getByRange(range: RangeTypeEnum, type: MovementTypeEnum): Observable<number> {
    return this.httpClient.get<number>(this.API_PRIVATE_URL + '/total?rangeType=' + range + '&type=' + type)
  }

  save(movement: MovementModel){
    return this.httpClient.post(this.API_PRIVATE_URL + '/save', movement);
  }

  getOne(movementId: string) : Observable<MovementModel>{
    return this.httpClient.get<MovementModel>(this.API_PRIVATE_URL + '/' + movementId)
  }

  delete(id: string): Observable<any> {
    return this.httpClient.delete(this.API_PRIVATE_URL + '/' + id)
  }

  getHistory(filter: CategoryGroupFilter) : Observable<Map<string,MovementTableModel[]>> {
    return this.httpClient.post<Map<string,MovementTableModel[]>>(this.API_PRIVATE_URL + '/history', filter)
  }

  uploadFile(fd: FormData){
   return this.httpClient.post( "api/private/v1/files", fd)
  }

}
