import {Injectable} from '@angular/core';
import {NgbDateAdapter, NgbDateStruct} from "@ng-bootstrap/ng-bootstrap";

@Injectable({
  providedIn: 'root'
})
export class CustomDateAdapter extends NgbDateAdapter<string> {

  readonly DELIMITER = '-';

  fromModel(value: string | null): NgbDateStruct | null {

    // console.log('fromModel value:', value)
    if (value) {
      let date = value.split(this.DELIMITER);
      return {
        year: parseInt(date[0], 10),
        month: parseInt(date[1], 10),
        day: parseInt(date[2], 10)
      };
    }
    return null;
  }

  fromModel2(date: string): NgbDateStruct | null {
    // console.log('fromModel value:', date)
    return (date && Number(date.substring(0, 4)) && Number(date.substring(5, 7) + 1) && Number(date.substring(8, 10))) ?
      {
        year: Number(date.substring(0, 4)),
        month: Number(date.substring(5, 7)),
        day: Number(date.substring(8, 10))
      } : null;
  }

  toModel_old(date: NgbDateStruct | null): string | null {
    // console.log('toModel value:: ', date)
    return date ?
      date.year + this.DELIMITER +
      this.pad(date.month) + this.DELIMITER +
      this.pad(date.day)
      : null;
  }

  toModel(date: NgbDateStruct): string | null {
    // console.log('toModel value:: ', date)
    return date ? date.year.toString() + '-' + String('00' + date.month).slice(-2)
      + '-' + String('00' + date.day).slice(-2) : null;
  }

  pad(number: number) {
    if (number < 10) {
      return '0' + number;
    }
    return number;
  }
}
