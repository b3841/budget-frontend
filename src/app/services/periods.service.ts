import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PeriodTableModel} from "../periods/models/period-table.model";
import {PeriodModel} from "../periods/models/period.model";

@Injectable({
  providedIn: 'root'
})
export class PeriodsService {

  private API_PRIVATE_URL = 'api/private/v1';

  constructor(private httpClient: HttpClient) {
  }

  getCurrentPeriod(): Observable<PeriodModel> {
    return this.httpClient.get<PeriodModel>(this.API_PRIVATE_URL + '/periods/current')
  }

  getCurrentOLatestPeriod(): Observable<PeriodModel> {
    return this.httpClient.get<PeriodModel>(this.API_PRIVATE_URL + '/periods/currentOrLatest')
  }

  getTable(): Observable<PeriodTableModel[]> {
    return this.httpClient.get<PeriodTableModel[]>(this.API_PRIVATE_URL + '/periods')
  }

  save(period: PeriodModel): Observable<any> {
    return this.httpClient.post(this.API_PRIVATE_URL + '/periods/save', period)
  }

  getOne(id: string): Observable<PeriodModel> {
    return this.httpClient.get<PeriodModel>(this.API_PRIVATE_URL + '/periods/' + id)
  }

  delete(id: string): Observable<any> {
    return this.httpClient.delete(this.API_PRIVATE_URL + '/periods/' + id)
  }
}
