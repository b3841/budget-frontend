import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {AccountTableModel} from "../accounts/models/account-table.model";
import {AccountModel} from "../accounts/models/account.model";

@Injectable({
  providedIn: 'root'
})
export class AccountsService {

  private API_PRIVATE_URL = 'api/private/v1';

  constructor(private httpClient: HttpClient) {
  }

  getTable(onlyDictionary: boolean = false): Observable<AccountTableModel[]> {
    return this.httpClient.get<AccountTableModel[]>(this.API_PRIVATE_URL + '/accounts?dict=' + onlyDictionary )
  }

  save(account: AccountModel): Observable<any> {
    return this.httpClient.post(this.API_PRIVATE_URL + '/accounts/save', account)
  }

  getOne(id: string): Observable<AccountModel> {
    return this.httpClient.get<AccountModel>(this.API_PRIVATE_URL + '/accounts/' + id)
  }

  delete(id: string): Observable<any> {
    return this.httpClient.delete(this.API_PRIVATE_URL + '/accounts/' + id)
  }
}
