import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {AccountTableModel} from "../accounts/models/account-table.model";
import {AccountModel} from "../accounts/models/account.model";
import {CategoryTableModel} from "../categories/models/category-table.model";
import {CategoryModel} from "../categories/models/category.model";
import {MovementTypeEnum} from "../models/enums/movement-type.enum";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  private URL = 'api/private/v1/categories';

  constructor(private httpClient: HttpClient) {
  }

  getTable(type: MovementTypeEnum): Observable<CategoryTableModel[]> {
    return this.httpClient.get<CategoryTableModel[]>( `${this.URL}?type=${type}`)
  }

  save(account: CategoryModel): Observable<any> {
    return this.httpClient.post(`${this.URL}/save`, account)
  }

  getOne(id: string, type: MovementTypeEnum): Observable<CategoryModel> {
    return this.httpClient.get<CategoryModel>(`${this.URL}/${id}?type=${type}`)
  }

  delete(id: string,  type: MovementTypeEnum): Observable<any> {
    return this.httpClient.delete(`${this.URL}/${id}?type=${type}`)
  }

  /*getParents(): Observable<CategoryModel[]> {
    return this.httpClient.get<CategoryModel[]>(this.URL + '/parents')
  }*/
}
