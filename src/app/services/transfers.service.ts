import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TransfersTableFilter} from "../transfers/models/transfer-table-filter.model";
import {TransferTableModel} from "../transfers/models/transfer-table.model";
import {TransferModel} from "../transfers/models/transfer.model";

@Injectable({
  providedIn: 'root'
})
export class TransfersService {
  private API_PRIVATE_URL = 'api/private/v1/transfers';

  constructor(private httpClient: HttpClient) {
  }

  getTable(filter: TransfersTableFilter): Observable<TransferTableModel[]> {
    return this.httpClient.post<TransferTableModel[]>(this.API_PRIVATE_URL, filter)
  }

  getOne(id: string) : Observable<TransferModel>{
    return this.httpClient.get<TransferModel>(this.API_PRIVATE_URL + '/' + id)
  }

  save(body: TransferModel) {
    return this.httpClient.post(this.API_PRIVATE_URL + '/save', body )
  }

  delete(id: string) {
    return this.httpClient.delete(this.API_PRIVATE_URL + '/' + id )
  }
}
