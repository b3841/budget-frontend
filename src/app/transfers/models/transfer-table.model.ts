export interface TransferTableModel {
  id: string,
  sourceAccount: string,
  sourceAccountBalanceBefore: number,
  sourceAccountBalanceAfter: number,
  targetAccount: string,
  targetAccountBalanceBefore: number,
  targetAccountBalanceAfter: number,
  date: string,
  amount: string,
  comment?: string,
  fee?: string
}
