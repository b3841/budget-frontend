import {RangeTypeEnum} from "../../models/enums/range-type.enum";

export interface TransfersTableFilter{
  rangeType: RangeTypeEnum,
  startDate?: string,
  endDate?: string,
  periodId?: string,
  sourceAccount: string,
  targetAccount: string,
}
