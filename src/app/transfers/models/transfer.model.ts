export interface TransferModel{
  id: string,
  sourceAccountId: string,
  targetAccountId: string,
  date: string,
  time: string,
  amount: number,
  comment?: string,
  fee?: number
}
