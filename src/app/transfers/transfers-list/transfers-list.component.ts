import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {AppToastService} from "../../_helpers/toast";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {
  NgbDateAdapter,
  NgbDatepicker,
  NgbDatepickerNavigateEvent,
  NgbModal,
  NgbModalRef
} from "@ng-bootstrap/ng-bootstrap";
import {LocalStorageService} from "../../services/local-storage.service";
import {CustomDateAdapter} from "../../services/adapters/ngb-date-adapter.service";
import {TransfersService} from "../../services/transfers.service";
import {TransfersTableFilter} from "../models/transfer-table-filter.model";
import {TransferTableModel} from "../models/transfer-table.model";
import {AccountsService} from "../../services/accounts.service";
import {AccountTableModel} from "../../accounts/models/account-table.model";
import {
  getLastDayOfMonth,
  getLastDayOfMonthFromString,
  getTodayString,
  pad,
  toIso1String
} from "../../_helpers/date.utils";
import {Observable} from "rxjs";
import {getTimeString} from "../../_helpers/time-utils";
import {DeleteComponent} from "../../shared_components/delete/delete.component";
import {RangeTypeEnum} from "../../models/enums/range-type.enum";
import {PeriodTableModel} from "../../periods/models/period-table.model";
import {PeriodsService} from "../../services/periods.service";

@Component({
  selector: 'app-transfers-list',
  templateUrl: './transfers-list.component.html',
  styleUrls: ['./styles.css'],
  providers: [
    {provide: NgbDateAdapter, useClass: CustomDateAdapter},
  ]
})
export class TransfersListComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private service: TransfersService,
              private periodService: PeriodsService,
              private accountService: AccountsService,
              private toast: AppToastService,
              private fb: FormBuilder,
              private modalService: NgbModal,
              private localStorage: LocalStorageService) {
  }

  @ViewChild('transferDialogTemplate')
  transferDialog: TemplateRef<any>;
  @ViewChild('transferDeleteModal')
  deleteDialog: TemplateRef<any>;
  activeModal: NgbModalRef;
  transferForm: FormGroup;
  transferId: string | null = null;
  selectedType: RangeTypeEnum = RangeTypeEnum.PERIOD
  @ViewChild("monthPicker")
  monthPicker: NgbDatepicker
  rangeType = RangeTypeEnum
  filterPeriods: PeriodTableModel[] = []
  filterAccounts: AccountTableModel[] = []

  filterForm: FormGroup
  transfers: TransferTableModel[] = []


  ngOnInit(): void {
    this.createFilterForm()
    this.getTable()
    this.getAccounts()
    this.getPeriods()
  }

  createFilterForm() {
    this.filterForm = this.fb.group({
      rangeType: [this.rangeType.PERIOD, []],
      startDate: [getTodayString(), []],
      endDate: [null, []],
      periodId: [this.localStorage.get("currentPeriod").id, []],
      accountId: [null, []],
      sourceAccountId: [null, []],
      targetAccountId: [null, []],
    })
  }

  createForm() {
    if (this.transferForm) this.transferForm.reset()
    else
      this.transferForm = this.fb.group({
        sourceAccountId: [null, [Validators.required]],
        targetAccountId: [null, [Validators.required]],
        date: [getTodayString(), [Validators.required]],
        time: [new Date().toLocaleTimeString(), [Validators.required]],
        amount: [null, [Validators.required]],
        comment: [null, []],
        fee: [0, []],
      })
  }

  getTable() {
    let filter: TransfersTableFilter = this.filterForm.getRawValue()

    console.log('filter', filter)
    this.service.getTable(filter)
      .subscribe(result => this.transfers = result,
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        }
      )
  }

  getAccounts() {
    this.accountService.getTable()
      .subscribe(
        (result) => {
          this.filterAccounts = result
        },
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        }
      )
  }

  getPeriods() {
    this.periodService.getTable()
      .subscribe(result => this.filterPeriods = result,
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        }
      )
  }

  onRangeTypeChange(e: string) {
    this.selectedType = e as RangeTypeEnum

    if (this.selectedType === RangeTypeEnum.MONTH) {
      this.filterForm.patchValue({
        startDate: toIso1String(this.monthPicker.state.firstDate),
        endDate: toIso1String(this.monthPicker.state.lastDate)
      })
    }

    if (this.selectedType === RangeTypeEnum.PERIOD) { // период по-умолчанию
      if (this.filterForm.get('startDate')?.value == null) return
    }

    if (this.selectedType === RangeTypeEnum.DIAPASON) {
      this.filterForm.patchValue({
        endDate: getLastDayOfMonthFromString(this.filterForm.get('startDate')?.value)
      })
    }

    if (this.selectedType === RangeTypeEnum.DAY) {
      this.filterForm.patchValue({
        endDate: null
      })
    }

    this.getTable()
  }

  onMonthPickerChange($event: NgbDatepickerNavigateEvent) {
    if ($event.current == null) return

    const m = $event.next.month
    const y = $event.next.year
    this.filterForm.patchValue({
      startDate: y + '-' + pad(m) + '-01',
      endDate: y + '-' + pad(m) + '-' + getLastDayOfMonth(pad(m), pad(y))
    })

    this.getTable()
  }


  new() {
    this.transferId = null
    this.activeModal = this.modalService.open(this.transferDialog, {centered: true});
    this.createForm()
  }

  edit(id: string) {
    this.new()

   this.service.getOne(id)
     .subscribe((value) =>{
       this.transferId = value.id
       this.transferForm.setValue({
         sourceAccountId: value.sourceAccountId,
         targetAccountId: value.targetAccountId,
         date: value.date,
         time: value.time,
         amount: value.amount,
         comment: value.comment,
         fee: value.fee
       })
     })
  }

  submit() {
    let form = this.transferForm.getRawValue()
    form.date = form.date + getTimeString()

    let body = {
      id: this.transferId,
      ...form
    }

    this.service.save(body)
      .subscribe(
        () => {
          this.activeModal.dismiss()
          this.getTable()
        },
        (err) => {
          console.log(err)
          this.toast.defaultError(err.error)
        }
      )
  }




  askDelete(id: string, sourceAccountId: string, targetAccountId: string, amount: string, comment: any) {
    const modalRef = this.modalService.open(DeleteComponent, {centered: true})
    const componentInstance = modalRef.componentInstance

    componentInstance.name = `"${sourceAccountId}-${targetAccountId} ${amount}, ${comment}`
    modalRef.result.then((deleto) => {
      if (deleto)
        this.service.delete(id)
          .subscribe(() => this.getTable(),
            (error) => {
              console.log(error)
              this.toast.defaultError(error.error)
            })
    })
  }
}
