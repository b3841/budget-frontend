export enum MovementTypeEnum {
  OUTCOME = "OUTCOME",
  INCOME = "INCOME"
}
