export enum CurrencyEnum{
  KZT = "KZT",
  USD = "USD",
  EUR = "EUR",
  UZS = "UZS",
  RUB = "RUB"
}
