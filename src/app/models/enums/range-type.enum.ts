export enum RangeTypeEnum {
  DAY = "DAY",
  WEEK = "WEEK",
  MONTH = "MONTH",
  PERIOD = "PERIOD",
  DIAPASON = "DIAPASON",
  ALL = "ALL"
}
