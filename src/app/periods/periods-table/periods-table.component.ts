import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {PeriodsService} from "../../services/periods.service";
import {PeriodTableModel} from "../models/period-table.model";
import {AppToastService} from "../../_helpers/toast";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbDateAdapter, NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {CustomDateAdapter} from "../../services/adapters/ngb-date-adapter.service";
import {PeriodModel} from "../models/period.model";

@Component({
  selector: 'app-periods-table',
  templateUrl: './periods-table.component.html',
  styleUrls: ['./periods-table.component.css'],
  // providers: [
  //   {provide: NgbDateAdapter, useClass: CustomDateAdapter},
  // ]
})
export class PeriodsTableComponent implements OnInit {

  items: PeriodTableModel[] = []
  @ViewChild('periodDialogTemplate')
  periodDialog: TemplateRef<any>;
  @ViewChild('periodDeleteModal')
  deleteDialog: TemplateRef<any>;
  activeModal: NgbModalRef;
  periodForm: FormGroup;
  periodId:  string | null = null;

  constructor(
    private periodsService: PeriodsService,
    private toast: AppToastService,
    private fb: FormBuilder,
    private modalService: NgbModal,
  ) {
  }

  ngOnInit(): void {
    this.getTable()
    this.createForm()
  }

  private getTable() {
    this.periodsService.getTable()
      .subscribe(
        (result) => {
          this.items = result
        },
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err?.error)
        }
      )
  }

  private createForm() {
    const datePattern = /(\d{4})-(0?[1-9]|1[0-2])-(0?[1-9]|[12]\d|30|31)/
    this.periodForm = this.fb.group({
      name: ['', [Validators.required]],
      startDate: ['', [Validators.required]],
      endDate: ['', [Validators.required]],
      comment: ['', []]
    })
  }

  // showModal() {
  //   this.activeModal = this.modalService.open(this.periodDialog, {centered: true});
  //
  //   this.activeModal.result.finally(() => this.periodForm.reset())
  // }

  submit() {
    const body: PeriodModel = {
      id: this.periodId,
      ...this.periodForm.getRawValue()
    }

    this.periodsService.save(body)
      .subscribe(
        (res) => {
          this.periodForm.reset()
          this.activeModal.dismiss()
          this.getTable()
        },
        (err) => {
          console.log(err)
          this.toast.defaultError(err.error)
        }
      )
  }

  edit(id: string) {
    this.new()

    this.periodsService.getOne(id)
      .subscribe(periodModel => {
          this.periodId = periodModel.id

          this.periodForm.setValue({
            name: periodModel.name,
            startDate: periodModel.startDate,
            endDate: periodModel.endDate,
            comment: periodModel.comment
          });
        },
        error => {
          console.log('err', error);
          this.toast.defaultError(error?.error)
        }
      )
  }

  new(){
    this.periodId = null
    this.activeModal = this.modalService.open(this.periodDialog, {centered: true});
    this.createForm()
  }


  askDelete(id: string) {
    this.modalService.open(this.deleteDialog, {centered: true})
      .result
      .then((res) => {
        if (res)
          this.periodsService
            .delete(id)
            .subscribe(
              () => this.getTable(),
              error => {
                console.log(error)
                this.toast.defaultError(error.error)
              })
      })
  }

}

