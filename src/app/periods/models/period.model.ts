export interface PeriodModel {
  id: string,
  name: string,
  startDate: string ,
  endDate: string ,
  comment: string
}
