export interface PeriodTableModel{

  id: string,
  range: string,
  name: string,
  comment?: string

}
