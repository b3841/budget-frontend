import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { TopBarComponent } from './top-bar/top-bar.component';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";
import { MainComponent } from './main/main.component';
import { BudgetsListComponent } from './budgets-list/budgets-list.component';
import { TransfersListComponent } from './transfers/transfers-list/transfers-list.component';
import { CategoriesListComponent } from './categories/categories-list/categories-list.component';
import { AccountsListComponent } from './accounts/accounts-list/accounts-list.component';
import { BalancesListComponent } from './balances-list/balances-list.component';
import { AccountsTableComponent } from './accounts/accounts-table/accounts-table.component';
import { PeriodsTableComponent } from './periods/periods-table/periods-table.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import {MaterialModule} from "./material.module";
import {ToastrModule} from "ngx-toastr";
import {NgSelectModule} from "@ng-select/ng-select";
import { DeleteComponent } from './shared_components/delete/delete.component';
import { MovementsListComponent } from './movements/movements-list/movements-list.component';
import { NewFromMainComponent } from './movements/new-from-main/new-from-main.component';
import { ShortInfoComponent } from './movements/short-info/short-info.component';
import {ImageUploaderComponent } from './shared_components/image_uploader/image-uploader.component';
import {FormatNumberPipe} from "./_helpers/pipes/FormatNumberPipe";

@NgModule({
  declarations: [
    AppComponent,
    TopBarComponent,
    MainComponent,
    BudgetsListComponent,
    TransfersListComponent,
    CategoriesListComponent,
    AccountsListComponent,
    BalancesListComponent,
    AccountsTableComponent,
    PeriodsTableComponent,
    DeleteComponent,
    MovementsListComponent,
    NewFromMainComponent,
    ShortInfoComponent,
    ImageUploaderComponent,
    FormatNumberPipe
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    ReactiveFormsModule,
    NgbModule,
    MaterialModule,
    FormsModule,
    NgSelectModule,
    RouterModule.forRoot([
      {path: '', component: MainComponent},
      {path: 'budgets', component: BudgetsListComponent},
      {path: 'transfers', component: TransfersListComponent},
      {path: 'categories', component: CategoriesListComponent},
      {path: 'accounts', component: AccountsListComponent},
      {path: 'balances', component: BalancesListComponent},
      {path: 'movements/:type', component: MovementsListComponent},

    ]),
    ToastrModule.forRoot({
      closeButton: true,
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
