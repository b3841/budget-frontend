import {Component, EventEmitter, Input, Output} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {MovementsService} from "../../services/movements.service";
import {AppToastService} from "../../_helpers/toast";


@Component({
  selector: 'app-image-uploader',
  templateUrl: './image-uploader.component.html',
  styleUrls: ['./image-uploader.component.css']
})
export class ImageUploaderComponent {

  @Output() selectedImageIds = new EventEmitter<string[]>()
  @Input() imageIds: Array<string> = []

  constructor(private movementService: MovementsService,
    private toast: AppToastService) {}

  public onFileSelected(event: Event) {

     const target = event.target as HTMLInputElement;
      if (target.files && target.files.length > 0) {
        this.upload(target.files[0]);
      }
  }

  upload(selectedFile: File) {
    const fd = new FormData();
    fd.append('file', selectedFile, selectedFile.name);

    this.movementService.uploadFile(fd)
      .subscribe((res: any) => {
            this.imageIds.push(res.fileId)
            this.selectedImageIds.emit(this.imageIds);
      }, (error: any) => {
      console.log(error)
          this.toast.defaultError(error?.error)
      });
  }

  deleteImage(imgId: string){
      this.imageIds.splice(this.imageIds.indexOf(imgId), 1);
      this.selectedImageIds.emit(this.imageIds)
  }
}
