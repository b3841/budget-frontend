import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.css']
})
export class DeleteComponent {


  @Input() name: string

  constructor(private activeModal: NgbActiveModal) { }

  close(deleto: boolean) {
    this.activeModal.close(deleto)
  }
}
