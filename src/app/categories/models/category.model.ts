import {MovementTypeEnum} from "../../models/enums/movement-type.enum";

export interface CategoryModel{
  id: string,
  name: string,
  parentId: string,
  type: MovementTypeEnum,
  storno: boolean
}
