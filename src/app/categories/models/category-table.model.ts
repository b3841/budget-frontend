import {MovementTypeEnum} from "../../models/enums/movement-type.enum";

export interface CategoryTableModel{
  id: string,
  name: string,
  parentId: string,
  formattedName: string,
  lastAction: string,
  storno: boolean,
  depth: number
}
