import {Component, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {CategoryTableModel} from "../models/category-table.model";
import {CategoriesService} from "../../services/categories.service";
import {AppToastService} from "../../_helpers/toast";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {MovementTypeEnum} from "../../models/enums/movement-type.enum";
import {Observable} from "rxjs";
import {CategoryModel} from "../models/category.model";
import {DeleteComponent} from "../../shared_components/delete/delete.component";

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css']
})
export class CategoriesListComponent implements OnInit {

  itemsOutcomes: CategoryTableModel[] = [];
  itemsIncomes: CategoryTableModel[] = [];
  parents: Observable<CategoryTableModel[]> | undefined

  @ViewChild('categoryDialogTemplate')
  categoryDialog: TemplateRef<any>;
  activeModal: NgbModalRef;
  categoryForm: FormGroup;
  movementType = MovementTypeEnum
  categoryId: string | null = null;

  constructor(
    private categoryService: CategoriesService,
    private toast: AppToastService,
    private fb: FormBuilder,
    private modalService: NgbModal
  ) {
  }

  ngOnInit(): void {
    this.getTables()
  }

  private getTables() {
    this.getTable(MovementTypeEnum.OUTCOME)
    this.getTable(MovementTypeEnum.INCOME)
  }

  private getTable(type: MovementTypeEnum) {

    this.categoryService.getTable(type)
      .subscribe(
        (result) => {
          if (type === MovementTypeEnum.OUTCOME)
            this.itemsOutcomes = result
          else
            this.itemsIncomes = result
        },
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        }
      )
  }

  edit(type: MovementTypeEnum, id: string) {
    this.new(type)

    this.categoryService.getOne(id, type)
      .subscribe((value) => {
        this.categoryId = value.id

        this.categoryForm.setValue(
          {
            name: value.name,
            parentId: value.parentId,
            type: value.type,
            storno: value.storno
          }
        )
      })
  }

  new(type: MovementTypeEnum) {
    this.categoryId = null
    this.parents = this.categoryService.getTable(type)
    this.activeModal = this.modalService.open(this.categoryDialog, {centered: true});
    this.createForm(type)
  }

  async askDelete(id: string, type: MovementTypeEnum) {
    const modalRef = this.modalService.open(DeleteComponent, {centered: true})
    const componentInstance = modalRef.componentInstance

    let categoryName: string
    let name: string

    if (type == MovementTypeEnum.OUTCOME) {
      categoryName = this.itemsOutcomes.find((item) => item.id === id)?.name
        ?? (await this.categoryService.getOne(id, type).toPromise()).name
      name = " расхода"
    } else {
      categoryName = this.itemsIncomes.find((item) => item.id === id)?.name
        ?? (await this.categoryService.getOne(id, type).toPromise()).name
      name = " дохода"
    }

    componentInstance.name = `категорию ${name} ${categoryName}`

    modalRef.result.then((deleto) => {
      if (deleto) {
        this.categoryService.delete(id, type)
          .subscribe(() => this.getTables(),
            (error) => {
              console.log(error)
              this.toast.defaultError(error.error)
            })
      }
    })
  }

  submit() {
    const body: CategoryModel = {
      id: this.categoryId,
      ...this.categoryForm.getRawValue()
    }

    this.categoryService.save(body)
      .subscribe(
        () => {
          this.activeModal.dismiss()
          this.getTables()
        },
        (err) => {
          console.log(err)
          this.toast.defaultError(err.error)
        }
      )
  }

  private createForm(type: string) {

    console.log('createForm: ', type)

    this.categoryForm = this.fb.group({
      name: [null, [Validators.required]],
      parentId: [null, []],
      type: [type, [Validators.required]],
      storno: [false, [Validators.required]]
    })
  }


}
