import {Component, EventEmitter, Input, OnInit, Output, TemplateRef, ViewChild} from '@angular/core';
import {MovementTypeEnum} from "../../models/enums/movement-type.enum";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Observable} from "rxjs";
import {CategoryTableModel} from "../../categories/models/category-table.model";
import {CategoriesService} from "../../services/categories.service";
import {AppToastService} from "../../_helpers/toast";
import {NgbDateAdapter, NgbModal, NgbModalRef} from "@ng-bootstrap/ng-bootstrap";
import {AccountTableModel} from "../../accounts/models/account-table.model";
import {PeriodTableModel} from "../../periods/models/period-table.model";
import {AccountsService} from "../../services/accounts.service";
import {PeriodsService} from "../../services/periods.service";
import {LocalStorageService} from "../../services/local-storage.service";
import {getTodayString, truncateMilliSeconds} from "../../_helpers/date.utils";
import {MovementModel} from "../models/movement.model";
import {MovementsService} from "../../services/movements.service";
import {CustomDateAdapter} from "../../services/adapters/ngb-date-adapter.service";
import {getTimeString} from "../../_helpers/time-utils";
import {tryCatch} from "rxjs/internal-compatibility";

type StaffVariant = 'from-movements' | 'from-accounts' | 'edit';

@Component({
  selector: 'app-new-from-main',
  templateUrl: './new-from-main.component.html',
  styleUrls: ['./styles.css'],
  providers: [
    {provide: NgbDateAdapter, useClass: CustomDateAdapter},
  ]
})
export class NewFromMainComponent implements OnInit {

  @Input() type: MovementTypeEnum
  @Input() variant: StaffVariant = 'from-movements'
  @Input() movementId: string | null = null
  @Input() accountId: string | null = null
  @Output() update: EventEmitter<boolean> = new EventEmitter()
  @ViewChild('movementDialogTemplate')
  movementDialog: TemplateRef<any>
  form: FormGroup
  activeModal: NgbModalRef;

  title: string
  movementType = MovementTypeEnum
  selectedFileIds: string[] // to upload
  movementImageIds: string[] // downloaded from server
  // movementId: string | null = null;

  categories: Observable<CategoryTableModel[]> | undefined
  accounts: Observable<AccountTableModel[]> | undefined
  periods: Observable<PeriodTableModel[]> | undefined

  constructor(
    private movementService: MovementsService,
    private categoryService: CategoriesService,
    private accountService: AccountsService,
    private periodService: PeriodsService,
    private toast: AppToastService,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private localStorage: LocalStorageService
  ) {
  }

  ngOnInit(): void {
    this.setTitle();
  }

  new() {
    this.categories = this.categoryService.getTable(this.type)
    this.accounts = this.accountService.getTable(true)
    this.activeModal = this.modalService.open(this.movementDialog, {centered: true})
    this.createForm()
  }

  private createForm() {
    this.form = this.fb.group({
      type: [this.type, [Validators.required]],
      categoryId: [null, [Validators.required]],
      accountId: [this.accountId, [Validators.required]],
      date: [getTodayString(), [Validators.required]],
      time: [new Date().toLocaleTimeString(), [Validators.required]],
      amount: [null, [Validators.required]],
      comment: [null, []],
      store: [null, []]
    })
  }

  private setTitle() {
    if (this.variant === 'from-movements')
      this.title = "Добавить"
    else if (this.type === MovementTypeEnum.OUTCOME)
      this.title = "Расход"
    else
      this.title = "Доход"
  }

  submit() {
    let form = this.form.getRawValue()
    form.date = form.date + getTimeString()

    let body = {
      id: this.movementId,
      fileIds: this.selectedFileIds,
      ...form
    }

    this.movementService.save(body)
      .subscribe(
        () => {
          this.activeModal.dismiss()
          this.update.emit(true)
        },
        (err) => {
          console.log(err)
          this.toast.defaultError(err.error)
        }
      )
  }

  edit() {
    this.new()

    this.movementService.getOne(this.movementId!!)
      .subscribe(movementModel => {
          this.form.setValue({
            type: this.type,
            categoryId: movementModel.categoryId,
            accountId: movementModel.accountId,
            date: movementModel.date,
            time: truncateMilliSeconds(movementModel.time),
            amount: movementModel.amount,
            comment: movementModel.comment,
            store: movementModel.store
          })

        this.movementImageIds = movementModel.fileIds
        },
        error => {
          console.log('err', error);
          this.toast.defaultError(error?.error)
        }
      )
  }

  onFileSelect(selectedImageIds: string[]){
    console.log(selectedImageIds, "selected")
    this.selectedFileIds = selectedImageIds
   }
}
