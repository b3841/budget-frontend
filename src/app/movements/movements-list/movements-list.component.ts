import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute} from '@angular/router'
import {CategoryGroupModel} from "../models/category-group.model";
import {MovementTableModel} from "../models/movement-table.model";
import {AppToastService} from "../../_helpers/toast";
import {FormBuilder, FormGroup} from "@angular/forms";
import {
  NgbDateAdapter,
  NgbDatepicker,
  NgbDatepickerNavigateEvent,
  NgbModal,
  NgbNavChangeEvent
} from "@ng-bootstrap/ng-bootstrap";
import {MovementsService} from "../../services/movements.service";
import {MovementTypeEnum} from "../../models/enums/movement-type.enum";
import {RangeTypeEnum} from "../../models/enums/range-type.enum";
import {CategoryGroupFilter} from "../models/category-group-filter";
import {CustomDateAdapter} from "../../services/adapters/ngb-date-adapter.service";
import {
  getLastDayOfMonth,
  getLastDayOfMonthFromString,
  getTodayString,
  pad,
  toIso1String
} from "../../_helpers/date.utils";
import {PeriodsService} from "../../services/periods.service";
import {PeriodTableModel} from "../../periods/models/period-table.model";
import {LocalStorageService} from "../../services/local-storage.service";
import {DeleteComponent} from "../../shared_components/delete/delete.component";
import {AccountTableModel} from "../../accounts/models/account-table.model";
import {AccountsService} from "../../services/accounts.service";
import {isTransfer, isTransferFee} from "../../_helpers/constants";
import {KeyValue} from "@angular/common";

@Component({
  selector: 'app-movements-list',
  templateUrl: './movements-list.component.html',
  styleUrls: ['./movements-list.component.css'],
  providers: [
    {provide: NgbDateAdapter, useClass: CustomDateAdapter},
  ]
})
export class MovementsListComponent implements OnInit {


  constructor(private route: ActivatedRoute,
              private service: MovementsService,
              private periodService: PeriodsService,
              private accountService: AccountsService,
              private toast: AppToastService,
              private fb: FormBuilder,
              private modalService: NgbModal,
              private localStorage: LocalStorageService
  ) {
  }

  active = 1
  movementType: MovementTypeEnum | undefined

  title2: string
  rangeType = RangeTypeEnum
  movementTypeEnum = MovementTypeEnum
  selectedType: RangeTypeEnum = RangeTypeEnum.PERIOD
  categories: CategoryGroupModel[] = [];
  movementsMap: Map<string, MovementTableModel[]>;
  historyMap: Map<string, MovementTableModel[]>;
  filterForm: FormGroup
  filterPeriods: PeriodTableModel[] = []
  filterAccounts: AccountTableModel[] = []
  @ViewChild("monthPicker")
  monthPicker: NgbDatepicker

  selectedCategoryId: string
  hoveredCategoryId: string


  ngOnInit(): void {
    this.route.params.subscribe(params => {
      this.movementType = params['type'];
      this.initComponent()

      if (this.movementType === MovementTypeEnum.OUTCOME)
        this.active = 1
      else if (this.movementType === MovementTypeEnum.INCOME)
        this.active = 2
      else this.active = 0
    });
  }

  initComponent() {
    this.getPeriods()
    this.getAccounts()
    this.createFilterForm()
    this.showTable()
  }

  /**
   * общая функция обновления движений или истории.
   * Начальная загрузка (в тч открытие таба) или изменения фильтра
   */
  showTable() {
    if (this.movementType)
      this.getTable()
    else
      this.getHistory()
  }

  /**
   * Обновление таблицы категории (первая загрузка и любые изменения фильтра)
   */
  getTable() {
    this.getCategoryGroups((result: CategoryGroupModel[]) => {
      this.categories = result
      this.movementsMap = new Map()
      this.selectedCategoryId = ''
    })
  }

  /**
   * Получение списка категорий
   * @param callback
   */
  getCategoryGroups(callback: (result: CategoryGroupModel[]) => any) {
    let filter: CategoryGroupFilter = this.filterForm.getRawValue()

    this.service.getTable(filter, this.movementType!!)
      .subscribe(result => callback(result),
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        }
      )
  }

  /**
   * Обновление таблицы журнала (первая загрузка, изменения фильтра и CRUD движений)
   */
  getHistory() {
    this.service.getHistory(this.filterForm.getRawValue())
      .subscribe(
        (result) => {
          this.historyMap = result
        },
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        }
      )
  }

  /**
   * Обновление таблиц после редактирования, удаления или создания движения
   */
  refreshTables() {
    if (this.movementType)
      this.getCategoryGroups((result: CategoryGroupModel[]) => {
        this.categories = result
        this.showMovements()
      })
    else
      this.getHistory()
  }

  onNavChange(changeEvent: NgbNavChangeEvent) {
    if (changeEvent.nextId === 1)
      this.movementType = MovementTypeEnum.OUTCOME
    else if (changeEvent.nextId === 2)
      this.movementType = MovementTypeEnum.INCOME
    else this.movementType = undefined

    this.showTable()
  }

  onRangeTypeChange(e: string) {
    this.selectedType = e as RangeTypeEnum

    if (this.selectedType === RangeTypeEnum.MONTH) {
      this.filterForm.patchValue({
        startDate: toIso1String(this.monthPicker.state.firstDate),
        endDate: toIso1String(this.monthPicker.state.lastDate)
      })
    }

    if (this.selectedType === RangeTypeEnum.PERIOD) { // период по-умолчанию
      if (this.filterForm.get('startDate')?.value == null) return
    }

    if (this.selectedType === RangeTypeEnum.DIAPASON) {
      this.filterForm.patchValue({
        endDate: getLastDayOfMonthFromString(this.filterForm.get('startDate')?.value)
      })
    }

    if (this.selectedType === RangeTypeEnum.DAY) {
      this.filterForm.patchValue({
        endDate: null
      })
    }

    this.showTable()
  }

  onMonthPickerChange($event: NgbDatepickerNavigateEvent) {
    if ($event.current == null) return

    const m = $event.next.month
    const y = $event.next.year
    this.filterForm.patchValue({
      startDate: y + '-' + pad(m) + '-01',
      endDate: y + '-' + pad(m) + '-' + getLastDayOfMonth(pad(m), pad(y))
    })

    this.showTable()
  }

  onCategorySelect($event: MouseEvent, categoryId: string, categoryName: string | null) {
    if (($event.target as HTMLElement).localName === 'a') {
      this.selectedCategoryId = categoryId

      this.showMovements()
    }
  }

  showMovements() {
    let filter: CategoryGroupFilter = this.filterForm.getRawValue()

    this.service.getMovements(this.selectedCategoryId, filter, this.movementType!!)
      .subscribe(
        (result) => {
          this.movementsMap = result
          // this.refreshMovements(null)
        },
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        }
      )
  }

  askDelete(id: string, comment: string, amount: number) {
    const modalRef = this.modalService.open(DeleteComponent, {centered: true})
    const componentInstance = modalRef.componentInstance

    componentInstance.name = `${this.title2} "${comment}" - ${amount}`
    modalRef.result.then((deleto) => {
      if (deleto)
        this.service.delete(id)
          .subscribe(() => this.refreshTables(),
            (error) => {
              console.log(error)
              this.toast.defaultError(error.error)
            })
    })
  }

  categoriesTotal(categoryGroups: CategoryGroupModel[]) {
    return categoryGroups
      .reduce((a, b) => a + b.own, 0)
  }

  movementsTotal(movements: MovementTableModel[]) {
    return movements
      .reduce((a, b) => a + b.amount, 0)
  }

  createFilterForm() {
    this.filterForm = this.fb.group({
      rangeType: [this.rangeType.PERIOD, []],
      startDate: [getTodayString(), []],
      endDate: [null, []],
      periodId: [this.localStorage.get("currentPeriod").id, []],
      accountId: [null, []]
    })
  }

  getPeriods() {
    this.periodService.getTable()
      .subscribe(result => this.filterPeriods = result,
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        }
      )
  }

  getAccounts() {
    this.accountService.getTable(true)
      .subscribe(result => this.filterAccounts = result,
        (err) => {
          console.log('err', err);
          this.toast.defaultError(err.error)
        })
  }

  isTransfer(movement: MovementTableModel) {
    // console.log(movement.comment + ', ' + movement.transfer)
    return movement.isTransfer || movement.isTransferFee
  }

  /**
   * Preventing keyvalue pipe from sorting already sorted map on BE
   */
  returnZero() {
    return 0;
  }
}
