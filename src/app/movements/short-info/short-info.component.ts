import { Component, OnInit } from '@angular/core';
import {MovementsService} from "../../services/movements.service";
import {AppToastService} from "../../_helpers/toast";
import {RangeTypeEnum} from "../../models/enums/range-type.enum";
import {MovementTypeEnum} from "../../models/enums/movement-type.enum";

@Component({
  selector: 'app-short-info',
  templateUrl: './short-info.component.html'
})
export class ShortInfoComponent implements OnInit {

  outcomesByDay: number = 0
  outcomesByPeriod: number = 0
  incomesByDay: number = 0
  incomesByPeriod: number = 0

  constructor(private movementsService: MovementsService,
              private toast: AppToastService) {
  }

  ngOnInit(): void {
    this.getValues()
  }

  private getValues() {
    this.movementsService.getByRange(RangeTypeEnum.DAY, MovementTypeEnum.OUTCOME).subscribe((result) => {
        this.outcomesByDay = result
      },
      (err) => {
        console.log('err', err);
        this.toast.defaultError(err.error)
      })
    this.movementsService.getByRange(RangeTypeEnum.PERIOD, MovementTypeEnum.OUTCOME).subscribe((result) => {
        this.outcomesByPeriod = result
      },
      (err) => {
        console.log('err', err);
        this.toast.defaultError(err.error)
      })

    this.movementsService.getByRange(RangeTypeEnum.DAY, MovementTypeEnum.INCOME).subscribe((result) => {
        this.incomesByDay = result
      },
      (err) => {
        console.log('err', err);
        this.toast.defaultError(err.error)
      })
    this.movementsService.getByRange(RangeTypeEnum.PERIOD, MovementTypeEnum.INCOME).subscribe((result) => {
        this.incomesByPeriod = result
      },
      (err) => {
        console.log('err', err);
        this.toast.defaultError(err.error)
      })


  }


}
