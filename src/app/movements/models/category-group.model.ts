export interface CategoryGroupModel {
  categoryId: string,
  parentCategoryId: string,
  depth: number,
  name: string,
  own: number,
  child: number
}
