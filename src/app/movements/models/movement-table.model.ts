import {MovementTypeEnum} from "../../models/enums/movement-type.enum";

export interface MovementTableModel {
  id: string,
  categoryName: string,
  categoryId: string,
  comment: string,
  accountName: string,
  accountId: string,
  date: string,
  time: string,
  amount: number,
  balance: number,
  type: MovementTypeEnum,
  isTransfer: Boolean,
  isTransferFee: Boolean,
  transferId?: string
}
