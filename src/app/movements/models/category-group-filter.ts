import {RangeTypeEnum} from "../../models/enums/range-type.enum";

export interface CategoryGroupFilter{
   rangeType: RangeTypeEnum,
   startDate?: string,
   endDate?: string,
   periodId?: string
}
