export interface MovementModel {
  id: string,
  categoryId: string,
  accountId: string,
  date: string,
  time: string,
  amount: number,
  comment: string,
  store: string,
  fileIds: string[]
}
