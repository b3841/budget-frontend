import {Component, OnInit} from '@angular/core';
import {PeriodsService} from "../services/periods.service";
import {LocalStorageService} from "../services/local-storage.service";
import {Observable} from "rxjs";
import {PeriodModel} from "../periods/models/period.model";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html'
})
export class MainComponent implements OnInit {

  constructor(
    private periodService: PeriodsService,
    private localStorageService: LocalStorageService
  ) {
  }

  ngOnInit(): void {
    this.getCurrentPeriod()
  }

  private getCurrentPeriod() {
    this.periodService.getCurrentOLatestPeriod().subscribe(
      result => {
        console.log(result)
        this.localStorageService.set('currentPeriod', result)
      }
    )
  }
}
